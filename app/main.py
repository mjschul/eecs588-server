from flask import Flask
from flask.ext.restful import reqparse, abort, Api, Resource, fields, marshal
from flask import request       # Post/Get data
from flask import make_response # Error handling
from time  import time
from werkzeug.routing import ValidationError
import peewee
from peewee import *
import hmac
import hashlib


BT_ADDRESS_LEN = 17 # Includes ":" in count

app = Flask(__name__)
app.config.from_pyfile('../settings.cfg')

# Email Mark errors ASAP if there is an error
if not app.debug:
    import logging
    from logging import Formatter
    from logging.handlers import SMTPHandler
    mail_handler = SMTPHandler('localhost',
                               'root@schultetwins.com',
                               app.config["ERROR_MAIL"], 'T^3 Failed in a serious way')
    mail_handler.setFormatter(Formatter('''
Message type:       %(levelname)s
Location:           %(pathname)s:%(lineno)d
Module:             %(module)s
Function:           %(funcName)s
Time:               %(asctime)s

Message:

%(message)s
'''))
    mail_handler.setLevel(logging.WARNING)
    app.logger.addHandler(mail_handler)
# End error/log handling

api = Api(app)
db = MySQLDatabase(app.config["DATABASE"], host = app.config["HOST"], user = app.config["USER"], password = app.config["PASS"])

class MySQLModel(peewee.Model):
    class Meta:
        database = db

# @TODO: Add Lat/Long to the Spot/SQL
class Spot(MySQLModel):
    id         = peewee.PrimaryKeyField(primary_key = True)
    MAC        = peewee.CharField(index = True)
    HashMAC    = peewee.CharField(index = True)
    RandMac    = peewee.BooleanField()
    FitBitId   = peewee.CharField(index = True, null = True)
    AccessAddr = peewee.CharField(index = True, null = True)
    SerialNum  = peewee.CharField(index = True, null = True)
    timestamp  = peewee.BigIntegerField()
    RSSI       = peewee.IntegerField(null = True)
    latitude   = peewee.DoubleField(help_text = "Location - latitude(in degrees, as a double)")
    longitude  = peewee.DoubleField(help_text = "Location - longitude(in degrees, as a double)")
    device     = peewee.IntegerField(help_text = "0 - Computer/raspberry pi, 1 - iOS, 2 - Android")
    created_by = peewee.CharField()

spot_fields = {
    'id': fields.Integer,
    'MAC': fields.String(attribute='HashMAC'),
    'RandMac': fields.Boolean,
    'FitBitId': fields.String,
    'AccessAddr': fields.String,
    'SerialNum': fields.String,
    'timestamp': fields.Integer,
    'RSSI': fields.Float,
    'latitude': fields.Float,
    'longitude': fields.Float,
    'device': fields.Integer,
    'created_by': fields.String
    }
spot_fields_list = {
    'MAC': fields.String(attribute='HashMAC'),
    'count': fields.Integer,
}

Spot.create_table(fail_silently = True)

def valid_bluetooth_addr(address):
    state = 'letter_number1'
    action = {'letter_number1': (lambda c: c >= '0' and c <= '9' or c >= 'A' and c <= 'F'),
             'letter_number2': (lambda c: c >= '0' and c <= '9' or c >= 'A' and c <= 'F'),
             'colon': (lambda c: c == ':')}
    next_state = {'letter_number1': 'letter_number2',
                  'letter_number2': 'colon',
                  'colon': 'letter_number1'}
    if not address or len(address.strip()) != BT_ADDRESS_LEN:
        msg = "{} Is not a valid bluetooth address".format(address)
        raise ValidationError(msg)
    address = address.strip().upper()
    for c in address:
        if not action[state](c):
            msg = "{} Is not a valid bluetooth address".format(address)
            raise ValidationError(msg)
        state = next_state[state]
    return address

def valid_timestamp(timestamp):
    if not timestamp:
        msg = "{} is more than a day away from the current time. Rejecting".format(timestamp)
        raise ValidationError(msg)
    timestamp = int(timestamp)
    my_timestamp = int(time())
    if abs((my_timestamp - timestamp) > 60*60*24):
        msg = "{} is more than a day away from the current time. Rejecting".format(timestamp)
        raise ValidationError(msg)
    return timestamp

def valid_loc(num, max_num, name):
    if not num:
        msg = "No {} given!".format(name)
        raise ValidationError(msg)
    num = float(num)
    if (abs(num) > max_num):
        msg = "{} is not a valid {}".format(num, name)
        raise ValidationError(msg)
    return num

def valid_latitude(latitude):
    return valid_loc(latitude, 85, "latitude")

def valid_longitude(longitude):
    return valid_loc(longitude, 180, "longitude")

def valid_device_type(device):
    types = {"computer": 0, "ios": 1, "android": 2}
    if not device:
        msg = "Null is not a valid device type!"
        raise ValidationError(msg)
    if device.strip().lower() not in types:
        msg = "{} is not a valid device type!".format(device)
        raise ValidationError(msg)
    device = device.strip().lower()
    return types[device]

class SpotListAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('timestamp', type=valid_timestamp, required=True)
        self.reqparse.add_argument('MAC', type=valid_bluetooth_addr, required=True)
        self.reqparse.add_argument('rand_mac', type=bool, required=True, help="Need 'rand_mac' field to tell us if MAC Address is random")
        self.reqparse.add_argument('name', type=str, required=True, help="Need 'name' field to tell us who provided data")
        self.reqparse.add_argument('RSSI', type=int, help="RSSI of reader")
        self.reqparse.add_argument('latitude', type=valid_latitude, required=True)
        self.reqparse.add_argument('longitude', type=valid_longitude, required=True)
        self.reqparse.add_argument('device', type=valid_device_type, required=True)
        self.reqparse.add_argument('passcode', help="Passcode not recognized", required=True)
        self.reqparse.add_argument('fitbitid', type=str)
        self.reqparse.add_argument('serialnum', type=str)
        self.reqparse.add_argument('access_addr', type=str)
        super(SpotListAPI, self).__init__()
        
    def get(self):
        result = []
        spots = Spot.select(Spot.HashMAC, fn.Count(Spot.MAC).alias('count')).group_by(Spot.HashMAC).execute()
        for spot in spots:
            result.append(spot)
        return marshal(result, spot_fields_list), 200

    def post(self):
        spot = {}
        args = self.reqparse.parse_args()
        if not args['passcode'] == app.config["PASSCODE"]:
            abort(400)
        try:
          last_spot_query = Spot.select().where(Spot.MAC == args.get('MAC')).order_by(Spot.id.desc()).limit(1)
          last_spot = last_spot_query.get()
        except Spot.DoesNotExist:
          last_spot = None

        if last_spot and (abs(int(args.get('timestamp')) - last_spot.timestamp) < 30):
          return {'result': 201}

        q = Spot.create(timestamp = args.get('timestamp'), MAC = args.get('MAC'),
                RandMac = args.get('rand_mac'), created_by = args.get('name'),
                RSSI = args.get('RSSI'), latitude = args.get('latitude'),
                longitude = args.get('longitude'), device = args.get('device'),
                FitBitId = args.get('fitbitid'), SerialNum = args.get('serialnum'),
                AccessAddr = args.get('access_addr'),
                HashMAC = hmac.new(app.config["HMAC_KEY"], args.get('MAC'), hashlib.sha1).hexdigest())
        return {'result': 200}

class SpotAPI(Resource):
    def get(self, HashMAC):
        result = []
        spots = Spot.select().where(Spot.HashMAC == HashMAC).execute()
        for spot in spots:
            result.append(spot)
        return marshal(result, spot_fields), 200

class Admin(Resource):
    def get(self):
        result = []
        spots = Spot.select(Spot.MAC, fn.Count(Spot.MAC).alias('count')).group_by(Spot.MAC).execute()
        for spot in spots:
            clean_up(spot.MAC)
        return {'result': 200}

class AdminMAC(Resource):
    def get(self):
        for spot in Spot.select():
            spot.HashMAC = hmac.new(app.config["HMAC_KEY"], spot.MAC, hashlib.sha1).hexdigest()
            spot.save()

api.add_resource(SpotListAPI, '/api/v1.0/spot')
api.add_resource(SpotAPI, '/api/v1.0/spot/<string:HashMAC>')
api.add_resource(Admin, '/admin/cleanup')
api.add_resource(AdminMAC, '/admin/update')

def clean_up(MAC):
    last_time_stamp = 0
    delete = []
    for spot in Spot.select().where(Spot.MAC == MAC).order_by(Spot.timestamp.asc()):
        if abs(last_time_stamp - spot.timestamp) < 30:
            delete.append(spot.id)
        else:
            last_time_stamp = spot.timestamp
    if not len(delete) == 0:
        Spot.delete().where(Spot.id << delete).execute()


@app.before_request
def before_request():
    db.connect()

@app.teardown_request
def tear_downrequest(exceptions=None):
    db.close()


if __name__ == "__main__":
    app.run(host='127.0.0.1', port=8080, debug=True)

# vim: expandtab tabstop=4 shiftwidth=4
