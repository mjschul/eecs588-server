
var map;
var markers = {};
var playMarkers;

function compare_MAC_list(a, b) {
  return a.count - b.count;
}

function loadAllMACS() {
  var xmlHttp = null;
  xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", "/api/v1.0/spot", false );
  xmlHttp.send( null );
  return JSON.parse(xmlHttp.responseText);
}

function loadbyMAC(MAC) {
  var xmlHttp = null;
  xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", "/api/v1.0/spot/" + MAC, false );
  xmlHttp.send( null );
  return JSON.parse(xmlHttp.responseText);
}

function addCheckbox(MAC, count) {
  $('#map-selectors').append('<label><input type="checkbox" name="MAC" value="' + MAC + '"/>' + MAC.substring(0, 8) + ' <span class="count">(' + count + ')</span></label><br />'); 
}

function deviceToName(device) {
  var mapping = ["computer (chrome)", "iOS", "Android"];
  return mapping[device];
}

function filterByDate(spot) {
//  if(document.getElementById("ondate").checked) {
    var values = $("#map-filter").slider("values");
    var ts = spot.timestamp;
    var begin = values[0];
    var end   = values[1];

    if(ts >= begin && ts <= end) {
      return true;
    }else{
      return false;
    }
//  }
//  return true;
}

function spotingText(spot) {
  var d = new Date(spot.timestamp * 1000);
  var str = '<div id="content">'+
      '<h2 id="SpotMacWindow">' + spot.MAC + '</h2>' +
      '<p>Timestamp: ' + d.toDateString() + ' ' + d.toTimeString() + '</p>' + 
      '<p>Device: ' + deviceToName(spot.device) + '</p></div>';
  return str;
}

function spotingMarkerShadow() {
  var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
      new google.maps.Size(40, 37),
      new google.maps.Point(0, 0),
      new google.maps.Point(12, 35));
  return pinShadow;
}

function rainbow(step) {
  var allColors = ["blue", "red", "green", "yellow", "orange", "purple", "pink"];
  return allColors[step % allColors.length];
}

function spotingMarker(pinColor) {
  var pinImage = new google.maps.MarkerImage("https://maps.google.com/mapfiles/ms/icons/" + pinColor + "-dot.png",
      new google.maps.Size(25, 34),
      new google.maps.Point(0,0),
      new google.maps.Point(10, 34));
  return pinImage;
}


function addSpoting(spot, icon, shadow) {
  var latlong = new google.maps.LatLng(spot.latitude, spot.longitude);
  var marker = new google.maps.Marker({
    position: latlong,
    map: map,
    title: spot.MAC,
    icon: icon,
    shadow: shadow
  });
  if (spot.MAC in markers) {
    markers[spot.MAC].push({spot: spot, marker: marker});
  }
  else {
    markers[spot.MAC] = [{spot: spot, marker: marker}];
  }
  var infowindow = new google.maps.InfoWindow({
    content: spotingText(spot)
  });
  google.maps.event.addListener(marker, 'click', function () {
    infowindow.open(map,marker);
  });
}

var colors = {};

function initialize() {
  var mapOptions = {
    center: new google.maps.LatLng(42.292850, -83.715446),
    zoom: 13
  };
  map = new google.maps.Map(document.getElementById("map-canvas"),
                              mapOptions);
  var all_macs = loadAllMACS();
  all_macs.sort(compare_MAC_list);
  all_macs.reverse();
  $.each(all_macs, function (index, element) {
    addCheckbox(element.MAC, element.count);
    colors[element.MAC] = rainbow(index);
  });
}

function loadMarkers(MAC) {
  var locations_by_mac = loadbyMAC(MAC);
  var icon = spotingMarker(colors[MAC]);
  var shadow = spotingMarkerShadow();
  $.each(locations_by_mac, function(index, element) {
    addSpoting(element, icon, shadow);
  });
  //connectMarkers(locations_by_mac);
}

function connectMarkers(locations) {
  $.each(locations, function(index, element) {
    alert(index+1 + " ?< " + locations.size());

    if(index+1 < locations.size()) {
      var next = locations.index(index+1);

      // Define a symbol using a predefined path (an arrow)
      // supplied by the Google Maps JavaScript API.
      var lineSymbol = {
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
      };

      // Create the polyline and add the symbol via the 'icons' property.

      var lineCoordinates = [
        new google.maps.LatLng(element.latitude, element.longitude),
        new google.maps.LatLng(next.latitude, next.longitude)
      ];

      var line = new google.maps.Polyline({
        path: lineCoordinates,
        icons: [{
          icon: lineSymbol,
          offset: '100%'
        }],
        map: map
      });

    }
  });
}

function killMarkers(MAC) {
  if (MAC in markers) {
    $.each(markers[MAC], function(index, element) {
      element.marker.setMap(null);
    });
    markers[MAC] = [];
  }
}

function reloadMarkers()
{
  $.each($("#map-selectors input"), function(index, element) {
    if($(this).is(":checked")) {
      killMarkers($(this).val());
      loadMarkers($(this).val());
    }
  });
}

function bubbleSort(a)
{
    var swapped;
    do {
        swapped = false;
        for (var i=0; i < a.length-1; i++) {
            if (a[i].spot.timestamp > a[i+1].spot.timestamp) {
                var temp = a[i];
                a[i] = a[i+1];
                a[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
}

function playMarkerSightings() {
  alert(markers.size());
  for(var i = 0; i < markers.length; i++) {
    alert(i);
    markers[i].marker.setMap(null);
  }

  //bubbleSort(markers); 

  //playMarkers = true;
  //displayMarkerSightings(0, 500);
}

function displayMarkerSightings(i, speed) {
  if(i < 0) {
    markers[i-1].marker.setMap(null);
  }

  if(i < markers.length && playMarkers) {
    markers[i].marker.setMap(map);
    setTimeout(function() { displayMarkerSightings(i+1, speed) }, speed);
  }
}

function slideTime(event, ui) {
  $("#begin-date").val(new Date(ui.values[0]*1000));
  $("#end-date").val(new Date(ui.values[1]*1000));
  $.each(markers, function(index, element) {
    $.each(element, function(index, element) {
      element.marker.setVisible(element.spot.timestamp >= ui.values[0] && element.spot.timestamp <= ui.values[1]);
    });
  });
}
function slideTimeTimeOnly(event, ui) {
  $("#begin-date").val(new Date(ui.values[0]*1000));
  $("#end-date").val(new Date(ui.values[1]*1000));
}

function playHistory() {
  var slide     = $("#map-filter")
  var slide_min = slide.slider("option", "min"); 
  var slide_max = slide.slider("option", "max"); 
  var time_wind = parseInt($("#time-window").val()); 
  var speed     = parseInt($("#time-speed").val()); 

  var slide_bot = slide.slider("values", 0);
  var slide_top = parseInt(slide_bot+time_wind);
  var step = 3600;

  //slide.slider("values", 0, slide_bot);
  slide.slider("values", 1, slide_top);

  playMarkers = true;
  incrementHistory(slide, step, speed);
}

function incrementHistory(slide, step, speed) {
  var slide_bot = slide.slider("values", 0) + step;
  var slide_top = slide.slider("values", 1) + step;
  var slide_max = slide.slider("option", "max");

  if(slide_top <= slide_max && playMarkers) {
    slide.slider("values", 0, slide_bot);
    slide.slider("values", 1, slide_top);
    playMarkers = setTimeout(function() {incrementHistory(slide, step, speed)}, speed);
  }else{
    playMarkers = false;
  }
} 

function pauseHistory() {
  playMarkers = false;
}

function resetHistory() {
  playMarkers = false;
  var slide     = $("#map-filter")
  var slide_min = slide.slider("option", "min"); 
  var slide_max = slide.slider("option", "max"); 
  slide.slider("values", 0, slide_min);
  slide.slider("values", 1, slide_max);
}
  

$(document).ready(function() {
  //$("#begin-date").datepicker();
  //$("#end-date").datepicker();
  var last_time = 0;
  $("#map-filter").slider({
    range: true,
    min: 1397015956,
    max: Math.round(new Date().getTime() / 1000),
    values: [1397015956 + 400, Math.round(new Date().getTime() / 1000) - 400],
    change: slideTime,
    slide: slideTimeTimeOnly
  });
  $('#checkall-mac').on("click", function () {
    if ($(this).is(":checked")) {
      $("#map-selectors input[type='checkbox'][name='MAC']").each(function(index, element) {
        if (!$(element).is(":checked")) {
          $(element).trigger("click");
        }
      });
    }
    else {
      $("#map-selectors input[type='checkbox'][name='MAC']").each(function(index, element) {
        if ($(element).is(":checked")) {
          $(element).trigger("click");
        }
      });
    }
  });
  $("#map-selectors").on("click", "input[type='checkbox'][name='MAC']", function() {
    if ($(this).is(":checked")) {
      loadMarkers($(this).val());
    }
    else {
      killMarkers($(this).val());
    }
  });
});

google.maps.event.addDomListener(window, 'load', initialize);
